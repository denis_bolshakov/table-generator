const table = document.getElementById("myTable"),
  columnCount = document.getElementById("columnCount"),
  rowCount = document.getElementById("rowCount"),
  htmlContainer = document.getElementById("htmlContainer");

class myTable {
  constructor(
    activeElement = false,
    activeRow = false,
    activeCell = false,
    controls = {}
  ) {
    (this.activeElement = activeElement),
      (this.activeRow = activeRow),
      (this.activeCell = activeCell),
      (this.controls = controls);
  }
  generateTable() {
    const columns = columnCount.value;
    const rows = rowCount.value;

    // clear table
    while (table.firstChild) {
      table.removeChild(table.firstChild);
    }

    // generate table without controls
    for (let i = 0; i < rows; i++) {
      const tr = table.insertRow(i);
      // generate cells
      for (let j = 0; j < columns; j++) {
        if (i === 0) {
          // generate th
          const th = document.createElement("th");
          th.setAttribute("data-row", i);
          th.setAttribute("data-cell", j);
          th.appendChild(document.createTextNode(j));
          tr.appendChild(th);
        } else {
          const td = tr.insertCell(j);
          td.setAttribute("data-row", i);
          td.setAttribute("data-cell", j);
          td.appendChild(document.createTextNode(j));
        }
      }
      table.appendChild(tr);
    }
    this.applyControls();
  }

  applyControls = function() {
    const keys = Object.keys(this.controls);
    const keysLength = keys.length;

    if (keysLength) {
      for (let i = 0; i < keysLength; i++) {
        const currentKey = Number(keys[i]);

        // current tr
        const tr = table.getElementsByTagName("tr")[currentKey];

        // objects of key
        const props = this.controls[currentKey];
        // controls for cells
        for (let key in props) {
          let currentCell;
          if (currentKey === 0) {
            currentCell = tr.getElementsByTagName("th")[key];
          } else {
            currentCell = tr.getElementsByTagName("td")[key];
          }
          const colspanStartPosition = props[key].colspanStartPosition,
            colspanEndPosition = props[key].colspanEndPosition,
            rowSpanStartPosition = props[key].rowSpanStartPosition,
            rowSpanEndPosition = props[key].rowSpanEndPosition;

          // apply colspan control for cell (if exist)
          if (colspanStartPosition !== colspanEndPosition) {
            // colspan value for cell
            let minusElements = colspanEndPosition - colspanStartPosition;
            let colspanNumber = minusElements + 1;
            currentCell.setAttribute("colspan", colspanNumber);
            while (minusElements !== 0) {
              tr.removeChild(tr.lastElementChild);
              --minusElements;
            }
          }
          // apply rowspan control for cell (if exist)
          if (rowSpanStartPosition !== rowSpanEndPosition) {
            let minusElements = rowSpanEndPosition - rowSpanStartPosition;
            let rowspanNumber = minusElements + 1;
            currentCell.setAttribute("rowspan", rowspanNumber);
            let nextTrNumber = currentKey + 1;
            let extraSpanned = colspanEndPosition - colspanStartPosition;

            while (minusElements !== 0) {
              const currentTr = table.getElementsByTagName("tr")[nextTrNumber];
              // delete extra cells when spanned cell
              if (extraSpanned !== 0) {
                for (let i = 0; i < extraSpanned + 1; i++) {
                  currentTr.removeChild(currentTr.lastElementChild);
                }
              } else {
                currentTr.removeChild(currentTr.lastElementChild);
              }
              ++nextTrNumber;
              --minusElements;
            }
          }
        }
      }
    }
    this.generateHtml();
  };

  setActiveElement = ev => {
    const elem = ev.target;
    if (elem.tagName === "TH" || elem.tagName === "TD") {
      const currentRow = elem.getAttribute("data-row");
      const currentCell = elem.getAttribute("data-cell");

      if (this.activeElement == false) {
        elem.style.background = "green";
        this.activeElement = true;
        // set active elements
        this.activeRow = currentRow;
        this.activeCell = currentCell;
      } else {
        // click to the active cell
        if (currentRow == this.activeRow && currentCell == this.activeCell) {
          elem.style.background = "#fff";
          this.activeElement = false;
          this.activeRow = false;
          this.activeCell = false;
          return;
        }

        this.activeElement = false;
        this.createContols(
          currentRow,
          currentCell,
          this.activeCell,
          this.activeRow
        );
        this.activeRow = false;
        this.activeCell = false;
      }
    }
  };

  createContols = (currentRow, currentCell, activeCell, activeRow) => {
    // colspan rowspan remake
    let rowSpanStartPosition,
      rowSpanEndPosition,
      colspanStartPosition,
      colspanEndPosition;

    // reverse if click to the previous row
    if (currentCell < this.activeCell) {
      colspanStartPosition = currentCell;
      colspanEndPosition = activeCell;
    } else {
      colspanStartPosition = activeCell;
      colspanEndPosition = currentCell;
    }

    if (currentRow < activeRow) {
      rowSpanStartPosition = currentRow;
      rowSpanEndPosition = activeRow;
    } else {
      rowSpanStartPosition = activeRow;
      rowSpanEndPosition = currentRow;
    }

    this.controls = {
      ...this.controls,
      [rowSpanStartPosition]: {
        ...this.controls[rowSpanStartPosition],
        [colspanStartPosition]: {
          rowSpanStartPosition,
          rowSpanEndPosition,
          colspanStartPosition,
          colspanEndPosition
        }
      }
    };
    this.generateTable();
  };
  generateHtml() {
    let html = "";

    html += "&lt;table&gt;";
    for (let i = 0; i < rowCount.value; i++) {
      html += "<br>  &lt;tr&gt;<br>";
      html += "    ";
      const currentTrElements = table.getElementsByTagName("tr")[i]
        .childElementCount;
      for (let j = 0; j < currentTrElements; j++) {
        // check props in whole tr and check props for cell
        if (
          this.controls.hasOwnProperty(i) &&
          this.controls[i].hasOwnProperty(j)
        ) {
          const curCellControls = this.controls[i][j],
            colspanStartPosition = curCellControls.colspanStartPosition,
            colspanEndPosition = curCellControls.colspanEndPosition,
            rowSpanStartPosition = curCellControls.rowSpanStartPosition,
            rowSpanEndPosition = curCellControls.rowSpanEndPosition;

          const hasColspanControls =
            colspanStartPosition !== colspanEndPosition;

          let colspanNumber = colspanEndPosition - colspanStartPosition + 1;

          const hasRowSpanControls =
            rowSpanStartPosition !== rowSpanEndPosition;

          let rowspanNumber = rowSpanEndPosition - rowSpanStartPosition + 1;

          if (i === 0) {
            html += `&lt;th&gt${j}&lt;/th&gt;`;
          } else {
            html += `&lt;td${
              hasColspanControls ? ` colspan="${colspanNumber}"` : ""
            }${
              hasRowSpanControls ? ` rowspan="${rowspanNumber}"` : ""
            }&gt${j}&lt;/td&gt;`;
          }
        } else {
          if (i === 0) {
            html += `&lt;th&gt${j}&lt;/th&gt;`;
          } else {
            html += `&lt;td&gt${j}&lt;/td&gt;`;
          }
        }
      }
      html += "<br>  &lt;/tr&gt;";
    }

    html += "<br>&lt;/table&gt;";

    htmlContainer.innerHTML = html;
  }
}

const tbl = new myTable();
tbl.generateTable();

table.addEventListener("click", e => {
  tbl.setActiveElement(e);
});
columnCount.addEventListener("change", () => {
  tbl.generateTable();
});
rowCount.addEventListener("change", () => {
  tbl.generateTable();
});
